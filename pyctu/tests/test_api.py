import datetime
import time
import binascii

from pyctu.commands.api_packet import APIPacket
from nose.tools import eq_

class TestAPIPacket(object):

    def setup(self):
        self._api = APIPacket('\x00\x13\xa2\x00\x40\x40\x11\x22')

    def test_create(self):
        pass

    def test_packet(self):
        """
        This packet is from p 108 of this manual:
        http://ftp1.digi.com/support/documentation/90000976_G.pdf

        7E 00 10 17 01 00 13 A2 00 40 40 11 22 FF FE 02 42 48 01 F5
        """
        packet = self._api.remote_api('BH', parameter='\x01')
        eq_(binascii.hexlify(packet), '7e001017010013a20040401122fffe02424801f5')

    def test_another_packet(self):
        """7e001017000013a2004030e5bbfffe02443304a9
           7e000e17020013a2004030e5bb02443305a3
        """
        self._api._frame_id = 
        packet = self._api.remote_api('D3', parameter='\x05')

    def test_get_length(self):
        eq_(
            self._api._get_length('\x17\x01\x00\x13\xA2\x00\x40\x40\x11\x22\xFF\xFE\x02\x42\x48\x01'),
            '\x00\x10'
        )

    def test_get_command(self):
        eq_(
            self._api._get_command('D4'),
            b'\x44\x34'
            )

    def test_checksum(self):
        """
        To calculate the check sum you add all bytes
        of the packet excluding the frame delimiter
        7E and the length (the 2nd and 3rd bytes).

        7E 00 0A 01 01 50 01 00 48 65 6C 6C 6F B8

        Add these Hex bytes:

        01 + 01 + 50 + 01 + 00 + 48 + 65 + 6C + 6C + 6F = 247

        Now take the result of 0x247 and keep only the lowest
        8 bits which in this example is 0x47 (the two far right
        digits). Subtract 0x47 from 0xFF and you get
        0xB8 (0xFF - 0x47 = 0xB8).
        0xB8 is the checksum for this data packet.
        """
        data = b'\x01\x01\x50\x01\x00\x48\x65\x6C\x6C\x6F'
        cs = self._api._get_checksum(data)
        eq_(cs, '\xB8')

    def test_local_api(self):
        """
        This packet is from p 103 of this manual:
        http://ftp1.digi.com/support/documentation/90000976_G.pdf

        7E 00 04 08 52 4E 4A 0D
        """
        self._api._frame_id = 0x51
        packet = self._api.local_api('NJ')
        eq_(binascii.hexlify(packet), '7e000408524e4a0d')

    def test_local_api_with_arg(self):
        self._api._frame_id = 0x51
        packet = self._api.local_api('D3', '\x04')
        eq_(binascii.hexlify(packet), '7e000508524433042a')


    def _test_local_api_parse(self):
        """7e000688184e490020a8"""
        val =self._api.parse('7e000688184e490020a8')
        eq_(val, '0020')
        
