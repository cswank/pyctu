import serial, time
from pyctu.commands.api import API
from pyctu.commands.api_packet import APIPacket
from xbee import XBee
import binascii

if __name__ == '__main__':
    p = APIPacket(None, None)
    p = API('/dev/ttyUSB0', 9600, p)
    print binascii.hexlify(p.send_local('D0'))
    print binascii.hexlify(p.send_local('D1'))
    print binascii.hexlify(p.send_local('D2'))
    print binascii.hexlify(p.send_local('D3'))
    print 'set:', binascii.hexlify(p.send_local('D3', '5'))
    #print binascii.hexlify(p.send_local('D4'))
    #print binascii.hexlify(p.send_local('D5'))
