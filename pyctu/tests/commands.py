import serial, time, binascii
from pyctu.commands.api import API
from pyctu.commands.api_packet import APIPacket
from xbee import XBee

if __name__ == '__main__':
    a1 = b'\x00\x13\xa2\x00\x40\x98\x25\xC1'
    a2 = b'\x00\x13\xa2\x00\x40\x98\x26\x3E'
    #ad_low = 0x409825c1
    #packet = APIPacket(0x13a200, 0x4098263E)
    p1 = APIPacket(a1)
    p2 = APIPacket(a2)
    p = API('/dev/ttyUSB0', 9600, p2)
    p.send('SL', packet=p1)
    print binascii.hexlify(p._readline())
