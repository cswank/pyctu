import serial, time
from pyctu.commands.api import API
from pyctu.commands.api_packet import APIPacket
from xbee import XBee

if __name__ == '__main__':
    a1 = '\x00\x13\xa2\x00\x40\x3D\xC7\x83' #my xbee
    p1 = APIPacket(a1)
    p = API('/dev/ttyUSB0', 9600, p1)
    time.sleep(2)
    p.send('D0', '\x02', p1)
    print p._readline()
    p.send('D1', '\x02', p1)
    print p._readline()
    p.send('IR', '\x32', p1)
    print p._readline()
    p.send('WR', packet=p1)
    print p._readline()
    p.send('D3', '\x04', packet=p1)
    print p._readline()
    
