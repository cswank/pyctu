import datetime
import time

from pyctu.xbee import Xbee
from nose.tools import eq_

class TestXbee(object):

    def setup(self):
        self._xbee = Xbee('/dev/tty.usbserial-A700eXQs')

    def teardown(self):
        self._xbee._ser.close()

    def test_create(self):
        pass

    def test_pan(self):
        eq_(self._xbee.pan, '6363')

    def test_address(self):
        eq_(self._xbee.address, '13A2004064203E')


        
