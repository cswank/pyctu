#! /usr/bin/python

"""
led_adc_example.py

By Paul Malmsten, 2010
pmalmsten@gmail.com

A simple example which sets up a remote device to read an analog value
on ADC0 and a digital output on DIO1. It will then read voltage 
measurements and write an active-low result to the remote DIO1 pin.
"""

from xbee import XBee
import serial
import time

ser = serial.Serial('/dev/ttyUSB0', 9600)
xbee = XBee(ser)

on_1 = dict(
    dest_addr_long='\x00\x13\xa2\x00\x40\x98\x25\xC1',
    command='D3',
    options='\x02',
    parameter='\x05'
    )

on_2 = dict(
    dest_addr_long='\x00\x13\xa2\x00\x40\x98\x26\x3E',
    command='D3',
    options='\x02',
    parameter='\x05'
    )

off_1 = dict(
    dest_addr_long='\x00\x13\xa2\x00\x40\x98\x25\xC1',
    command='D3',
    options='\x02',
    parameter='\x04'
    )

off_2 = dict(
    dest_addr_long='\x00\x13\xa2\x00\x40\x98\x26\x3E',
    command='D3',
    options='\x02',
    parameter='\x04'
    )

for i in xrange(100):
    if i % 2:
        xbee.remote_at(**on_1)
        xbee.remote_at(**off_2)
    else:
        xbee.remote_at(**off_1)
        xbee.remote_at(**on_2)
    time.sleep(0.05)

ser.close()
