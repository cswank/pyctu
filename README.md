Pyctu - a XBee configuration tool
--------------------------------------------------------------------------------

I like to use XBee modules, but I don't like to use Windows.  If you
want to configure your XBee module the choices are XCTU, a Windows
GUI, or use AT commands from a serial emulator.  If you want to update
or change the firmware the only tool is XCTU.

Pyctu aims to provide the functionality of XCTU from Linux.  As of now
it can configure modules that have AT firmware and API firmware as
well.  It can also do firmware updates.

However, everything doesn't work perfectly and the curses-based UI is
probably difficult for someone (besides me) to use.  I'm putting Pyctu
package out there to see if anyone is interested in using it, and hopefully
to get feedback or contributions that will improve it.

To use pyctu you need to plug your XBee into a serial to USB adaptor and
type (assuming your adaptor is mounted at /dev/ttyUSB0)::

    $ pyctu /dev/ttyUSB0

Now you should get the first screen of the interface::

    ┌─────────────────────────────────────────────────────────────────────────────┐
    │  networking  rf  serial  tools                                              │
    │_____________________________________________________________________________│
    │  address_high:                        13A200                                │
    │  address_low:                         409825C1                              │
    │  aes_encryption_enable:               0                                     │
    │  aes_key:                             OK                                    │
    │  channel:                             11                                    │
    │  coordinator:                         ERROR                                 │
    │  destination_address_high:            0                                     │
    │  destination_address_low:             0                                     │
    │  mac_mode:                            ERROR                                 │
    │  node_discover:                       <enter>                               │
    │  node_id:                                                                   │
    │  pan:                                 0                                     │
    │  source_address:                      34BD                                  │
    │  xbee_retries:                        ERROR                             
    └─────────────────────────────────────────────────────────────────────────────┘        

Now, if you wanted to change, say, the pan id, you would hit the down arrow
until the value for pan was highlighted and then push 'enter'.  You will now
have a popup for entering a pan id::

    ┌─────────────────────────────────────────────────────────────────────────────┐
    │  networking  rf  serial  tools                                              │
    │_____________________________________________________________________________│
    │  address_high:                        13A200                                │
    │  address_low:                         409825C1                              │
    │  aes_encryption_enable:               0                                     │
    │  aes_key:                             OK                                    │
    │  channel:                             11                                    │
    │  coordinator:                         ERROR                                 │
    │  destination_address_high:            0                                     │
    │  destination_address_low:             0                                     │
    │  mac_mode:       ┌──────────────────────────────────────┐                   │
    │  node_discover:  │                 pan                  │                   │
    │  node_id:        │22                                    │                   │
    │  pan:            └──────────────────────────────────────┘                   │
    │  source_address:                      34BD                                  │
    │  xbee_retries:                        ERROR                                 
    └─────────────────────────────────────────────────────────────────────────────┘

I'll be adding more documentation, but for now I just want to see if anyone is
interested in this thing.